package com.news.app.home

import androidx.lifecycle.ViewModel
import com.news.app.data.News
import com.news.app.data.NewsRepository

class HomeViewModel(private val repository: NewsRepository) : ViewModel() {
    // TODO: Implement the ViewModel

    var onItemClickListener: OnNewsClickListener? = null

    suspend fun getNews(country:String): List<NewsViewModel> =
         repository.getNews(country)
             .map { news -> NewsViewModel(news) }


    inner class NewsViewModel(val news: News) {

        fun onItemClicked() {
            onItemClickListener?.invoke(news)
        }
    }

}

typealias OnNewsClickListener = (news: News) -> Unit
