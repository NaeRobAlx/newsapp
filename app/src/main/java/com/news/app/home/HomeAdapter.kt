package com.news.app.home


import android.view.LayoutInflater
import android.view.ViewGroup

import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.news.app.R
import com.news.app.databinding.HomeItemBinding



class HomeAdapter : ListAdapter<HomeViewModel.NewsViewModel, HomeAdapter.NewsViewHolder>(DiffCallback()) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = HomeItemBinding.inflate(layoutInflater)
        return NewsViewHolder(binding)
    }


    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
        val day = getItem(position)



        holder.bind(day)
     }


    inner class NewsViewHolder(private val binding: HomeItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(news: HomeViewModel.NewsViewModel) {
            binding.newsViewModel = news

            Glide.with(this.itemView)
                .load(news.news.urlToImage)
                .fitCenter()
                .placeholder(R.drawable.no_image)
                .override(500,200)
                .into(binding.imageView)

        }


    }


    private class DiffCallback : DiffUtil.ItemCallback<HomeViewModel.NewsViewModel>() {
        override fun areItemsTheSame(oldItem: HomeViewModel.NewsViewModel, newItem: HomeViewModel.NewsViewModel): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: HomeViewModel.NewsViewModel, newItem: HomeViewModel.NewsViewModel): Boolean {
            return oldItem.news.title == newItem.news.title
        }
    }

}