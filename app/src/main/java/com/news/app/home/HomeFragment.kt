package com.news.app.home

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope

import com.news.app.databinding.HomeFragmentBinding
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject
import androidx.navigation.fragment.findNavController


class HomeFragment : Fragment() {

    companion object {
        fun newInstance() = HomeFragment()
    }

    private val viewModel: HomeViewModel by inject()
    private lateinit var binding: HomeFragmentBinding
    private val adapter = HomeAdapter()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = HomeFragmentBinding.inflate(layoutInflater)
        binding.newsList.adapter = adapter



        if (adapter.itemCount == 0)
        {
            // Fetch the news async
            lifecycleScope.launch {
                adapter.submitList(viewModel.getNews("us"))

            }
        }
        else
        {
            adapter.notifyDataSetChanged()
        }


        // We set a listener to receive callbacks whenever an item is clicked
        viewModel.onItemClickListener = onItemClickListener


        binding.searchBtn.setOnClickListener{
             val directions = HomeFragmentDirections.toSearchFragment("")
             findNavController().navigate(directions)
        }



        return binding.root
    }


    private val onItemClickListener: OnNewsClickListener = { news ->
        // Navigate to the details page
//        Log.w("click", "id = (${news.id}) title = "+news.title)


        val directions = HomeFragmentDirections.toDetailsFragment(news.title);
        findNavController().navigate(directions)
     }


}
