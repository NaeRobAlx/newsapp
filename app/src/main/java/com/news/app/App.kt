package com.news.app

import android.app.Application
import androidx.room.Room
import com.news.app.data.NewsRepository
import com.news.app.data.WebService
import com.news.app.home.HomeViewModel
import com.news.app.NewsDatabase.Companion.DATABASE_NAME
import com.news.app.details.DetailsViewModel
import com.news.app.search.SearchViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.dsl.module


class App : Application() {

    override fun onCreate() {
        super.onCreate()

        // Initialize dependency injection
        startKoin {
            androidContext(this@App)
            modules(appModule)
        }
    }

    // Module containing all project dependencies
    private val appModule = module {
        single { Room.databaseBuilder(get(), NewsDatabase::class.java, DATABASE_NAME).build() }
        single { get<NewsDatabase>().newsDao() }
        single { WebService() }
        single { NewsRepository(get(), get()) }
        viewModel { HomeViewModel(get()) }
        viewModel { SearchViewModel(get()) }
        viewModel { DetailsViewModel(get()) }

    }
}