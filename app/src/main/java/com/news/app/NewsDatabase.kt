package com.news.app

import androidx.room.Database
import androidx.room.RoomDatabase
import com.news.app.data.News
import com.news.app.data.NewsDao


@Database(entities = [News::class], version = 1, exportSchema = false)
 abstract class NewsDatabase : RoomDatabase() {

    abstract fun newsDao(): NewsDao?

    companion object {
        const val DATABASE_NAME: String = "news.db"
    }
}