package com.news.app

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.view.animation.AlphaAnimation
import com.news.app.databinding.ActivityMainBinding
import com.news.app.databinding.ActivitySplashBinding
import kotlinx.android.synthetic.main.activity_splash.*

class Splash : AppCompatActivity() {

    private val SPLASH_TIME_OUT = 4000L
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding.root)

        name1.fadeIn(durationMillis = 1000)
        name2.fadeIn(durationMillis = 1000)
        name3.fadeIn(durationMillis = 1000)

        Handler().postDelayed(
            {
                val i = Intent(this@Splash, MainActivity::class.java)
                startActivity(i)
                finish()
            }, SPLASH_TIME_OUT)
    }


    inline fun View.fadeIn(durationMillis: Long = 250) {
        this.startAnimation(AlphaAnimation(0F, 1F).apply {
            duration = durationMillis
            fillAfter = true
        })
    }

    inline fun View.fadeOut(durationMillis: Long = 250) {
        this.startAnimation(AlphaAnimation(1F, 0F).apply {
            duration = durationMillis
            fillAfter = true
        })
    }
}
