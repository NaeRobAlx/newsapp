package com.news.app.search

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.news.app.R
import com.news.app.databinding.SearchItemBinding

class SearchAdapter : ListAdapter<SearchViewModel.NewsViewModel, SearchAdapter.NewsViewHolder>(DiffCallback()) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = SearchItemBinding.inflate(layoutInflater)
        return NewsViewHolder(binding)
    }


    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
        val day = getItem(position)

        holder.bind(day)
    }


    inner class NewsViewHolder(private val binding: SearchItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(news: SearchViewModel.NewsViewModel) {
            binding.searchViewModel = news

            Glide.with(this.itemView)
                .load(news.news.urlToImage)
                .placeholder(R.drawable.no_image)
                .override(500,200)
                .into(binding.imageView)

        }


    }


    private class DiffCallback : DiffUtil.ItemCallback<SearchViewModel.NewsViewModel>() {
        override fun areItemsTheSame(oldItem: SearchViewModel.NewsViewModel, newItem: SearchViewModel.NewsViewModel): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: SearchViewModel.NewsViewModel, newItem: SearchViewModel.NewsViewModel): Boolean {
            return oldItem.news.title == newItem.news.title
        }
    }

}