package com.news.app.search

import android.app.Activity
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import org.koin.android.ext.android.inject

import com.news.app.databinding.SearchFragmentBinding
import kotlinx.coroutines.launch
import java.lang.Exception

class SearchFragment : Fragment() {

    companion object {
        fun newInstance() = SearchFragment()
    }

    private  val viewModel: SearchViewModel by inject()
    private lateinit var binding: SearchFragmentBinding
    private val adapter = SearchAdapter()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = SearchFragmentBinding.inflate(layoutInflater)
        binding.newsList.adapter = adapter



        binding.searchBtn.setOnClickListener {

            searchNews()

        }

        binding.queryLabel.setOnEditorActionListener { _, actionId, _ ->
            when (actionId) {
                EditorInfo.IME_ACTION_DONE -> {
                    searchNews()
                    true
                }
                else -> false
            }
        }

        binding.close.setOnClickListener {
            activity?.onBackPressed()
        }

        viewModel.onItemClickListener = onItemClickListener

        return binding.root
    }

    private fun searchNews( )
    {

        try {
            val inputMethodManager = activity?.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(binding.queryLabel.windowToken, 0)
        }
        catch (e:Exception)
        {
            e.printStackTrace()
        }

        val query = binding.queryLabel.text.toString();

        if (query.isEmpty()) {
            Toast.makeText(activity,"Please enter your text before you press search!",Toast.LENGTH_SHORT).show()

        } else {

            // Fetch the news async
            lifecycleScope.launch {
                adapter.submitList(viewModel.getNewsByQuery(query))

            }
        }
    }

    private val onItemClickListener: OnNewsClickListener = { news ->
        // Navigate to the details page
//        Log.w("click", "id = (${news.id}) title = "+news.title)
        val directions = SearchFragmentDirections.toDetailsFragment(news.title);
        findNavController().navigate(directions)

    }

}
