package com.news.app.details

import androidx.lifecycle.ViewModel
import com.news.app.data.News
import com.news.app.data.NewsRepository

class DetailsViewModel(private val repository: NewsRepository) : ViewModel() {
    // TODO: Implement the ViewModel


    suspend fun getNews(title: String): News? = repository.getNewsByTitle(title)

}
