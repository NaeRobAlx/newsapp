package com.news.app.details

import android.content.Intent
import android.net.Uri
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide

import com.news.app.R
import com.news.app.data.News
import com.news.app.databinding.DetailsFragmentBinding
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject

class DetailsFragment : Fragment() {

    companion object {
        fun newInstance() = DetailsFragment()
    }
    private val args: DetailsFragmentArgs by navArgs()

    private  val viewModel: DetailsViewModel by inject()
    private lateinit var binding: DetailsFragmentBinding

    private var news: News? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DetailsFragmentBinding.inflate(layoutInflater)

         lifecycleScope.launch {
            news = viewModel.getNews(args.title)
            binding.news = news

            Glide.with(this@DetailsFragment)  //2
                .load(news?.urlToImage) //3
                .placeholder(R.drawable.no_image)
                .override(500,200)
                .into(binding.imageView2)
        }


        binding.shareBtn.setOnClickListener {

            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.type = "text/plain"
            shareIntent.putExtra(Intent.EXTRA_TEXT, news?.url)
            startActivity(Intent.createChooser(shareIntent,"Share via"))
        }

        binding.openSite.setOnClickListener {
            val openURL = Intent(Intent.ACTION_VIEW)
            openURL.data = Uri.parse(news?.url)
            startActivity(openURL)
        }

        binding.close.setOnClickListener {
            activity?.onBackPressed()
        }


        return binding.root
    }



}
