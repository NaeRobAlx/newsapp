package com.news.app.data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface NewsDao {

    @Query("SELECT * FROM News")
    suspend fun getNews(): List<News>

    @Insert
    suspend fun saveNews(list: List<News>)

    @Query("DELETE FROM News")
    suspend fun clearNews()


}