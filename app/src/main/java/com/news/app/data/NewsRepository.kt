package com.news.app.data

class NewsRepository(
    private val newsDao: NewsDao,
    private val webService: WebService
 ) {

    suspend fun getNews(country:String): List<News> {

            val newsData = try {
                webService.getNews(country).news

            } catch (ex: Exception) {
                null
            }

             newsData?.map { it.toNews() }
                ?.also {
                     newsDao.clearNews()
                     newsDao.saveNews(it)
                  }

         return newsDao.getNews()
    }



    suspend fun getNewsByQuery(query:String): List<News> {

        val newsData = try {
            webService.getNewsByQuery(query).news

        } catch (ex: Exception) {
            null
        }

        var data:List<News> = emptyList()

         newsData?.map { it.toNews() }
            ?.also {

                newsDao.saveNews(it)
                data = it


            }

        return data
    }

    //get specific news
    suspend fun getNewsById(id: Int): News? =
        newsDao.getNews().firstOrNull { it.id == id }

    //get specific news
    suspend fun getNewsByTitle(title: String): News? =
        newsDao.getNews().firstOrNull { it.title == title }


}