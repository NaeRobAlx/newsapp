package com.news.app.data

import com.google.gson.GsonBuilder
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

class WebService {

    private val api: NewsApi by lazy {

        val gson = GsonBuilder()
            .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
            .create()

        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
            .create(NewsApi::class.java)
    }

    suspend fun getNews(country:String): NewsList = api.getHeadlines(country)
    suspend fun getNewsByQuery(query:String): NewsList = api.getNewsByQuery(query)


    /**
     * Retrofit instance which holds details about the API calls.
     */
    interface NewsApi {

        /**
         * Resulting URL: http://newsapi.org/v2/top-headlines?country=ro&apiKey=c246b9e0f58c47928f1d34f3ae151d79
         */
        @GET("top-headlines")
        suspend fun getHeadlines(
            @Query("country") query: String,
            @Query("apiKey") apiKey: String = API_KEY
        ): NewsList


        //http://newsapi.org/v2/everything?q=bitcoin&apiKey=c246b9e0f58c47928f1d34f3ae151d79
        @GET("everything")
        suspend fun getNewsByQuery(
            @Query("q") query: String,
            @Query("apiKey") apiKey: String = API_KEY)
                : NewsList


    }

    companion object {
        const val BASE_URL = "http://newsapi.org/v2/"
        const val API_KEY = "c246b9e0f58c47928f1d34f3ae151d79"


    }
}