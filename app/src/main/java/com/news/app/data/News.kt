package com.news.app.data

import androidx.annotation.Nullable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

data class NewsList(
    @SerializedName("articles")
    val news: List<NewsData>
)

data class NewsData(

    @SerializedName("author")
    val author:String = "",
    @SerializedName("title")
    val title:String,
    @SerializedName("description")
    val description:String,
    @SerializedName("url")
    val url:String,
    @SerializedName("urlToImage")
    val urlToImage:String,
    @SerializedName("publishedAt")
    val publishedAt:String,
    @SerializedName("content")
    val content:String,
    @SerializedName("source")
    val source: Source


)
{
    val authorName get():String = if (author == null) "" else author
    val sourceData get():Source = if (source == null) Source("")  else source
    val descriptionData get():String = if (description == null) ""  else description
    val contentData get():String = if (content == null) descriptionData  else content
    val urlToImgData get():String = if (urlToImage == null) ""  else urlToImage

}

data class Source(
    @SerializedName("name")
    val name:String
)

@Entity(tableName = "News")
data class News(
    @PrimaryKey(autoGenerate = true)
    val  id:Int,
    @ColumnInfo(name = "author")
    val author: String = "",
    @ColumnInfo(name = "title")
    val title: String,
    @ColumnInfo(name = "description")
    val description: String,
    @ColumnInfo(name = "url")
    val url: String,
    @ColumnInfo(name = "urlToImage")
    val urlToImage: String,
    @ColumnInfo(name = "publishedAt")
    val publishedAt: String,
    @ColumnInfo(name = "content")
    val content: String,
    @ColumnInfo(name = "source")
    val source: String

)

fun NewsData.toNews(): News =
    News(
        id = 0,
        author = authorName,
        title = title,
        description = descriptionData,
        url = url,
        urlToImage = urlToImgData,
        publishedAt = publishedAt,
        content = contentData,
        source = sourceData.name
    )